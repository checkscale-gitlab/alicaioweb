[![Netlify Status](https://api.netlify.com/api/v1/badges/6482d92e-46f3-4cb3-9baa-e03eb805f17e/deploy-status)](https://app.netlify.com/sites/alicaioweb/deploys)
[![pipeline status](https://gitlab.com/Ollaww/alicaioweb/badges/master/pipeline.svg)](https://gitlab.com/Ollaww/alicaioweb/commits/master)

# Description

Simple blog website developed using [Jekyll](https://jekyllrb.com/).
<br>
<br>
Latest master deploy avaiable [on netlify](https://alicaioweb.netlify.app/).

## Debug and Test

To run on your own machine:
 - verify that *host* on `_config.yml` is set to `0.0.0.0`
 - Run `docker-compose up`. 
 - Open <http:localhost:4000>
